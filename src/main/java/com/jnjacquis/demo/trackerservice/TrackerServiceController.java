package com.jnjacquis.demo.trackerservice;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TrackerServiceController {

    private static final Logger LOGGER = LoggerFactory.getLogger(TrackerServiceController.class);

    @GetMapping("/track")
    public boolean track(@RequestParam(value = "id") int itemId) {
        LOGGER.info("Set tracking of item with id {}", itemId);
        return true;
    }

    @GetMapping("/untrack")
    public boolean untrack(@RequestParam(value = "id") int itemId) {
        LOGGER.info("Set untracking of item with id {}", itemId);
        return true;
    }
}
